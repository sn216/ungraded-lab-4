import java.util.Scanner;
public class Application {
    
    public static void main (String args[])
    {
        Scanner scan = new Scanner(System.in);
        int amountStudiedPos = scan.nextInt();
        int amountStudiedNeg = scan.nextInt();

        Students x = new Students();
        x.setName("Sriraam");
        x.studentsAge = 18;
        x.studentsWithPc = "Yes";

        Students y = new Students();
        y.setName("Ryan");
        y.studentsAge = 17;
        y.studentsWithPc = "No";

        Students z = new Students();
        z.setName("Davis");
        z.studentsAge = 18;
        z.studentsWithPc = "Yes";

        Students[] section3 = new Students[3];
        section3[0] = x;
        section3[1] = y;
        section3[2] = z;
        section3[1].learn(amountStudiedPos);
        section3[2].learn(amountStudiedNeg);
   
        System.out.println(x.studentName);



    }
}
